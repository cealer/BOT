﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BotPwa.Models
{
    public class Company
    {
        public string comId { get; set; }
        public string comDescription { get; set; }
        public DateTime comDate { get; set; }
        public string userId { get; set; }
        public bool comEnabled { get; set; }
    }
}