﻿using com.valgut.libs.bots.Wit;
using com.valgut.libs.bots.Wit.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BotPwa.Dialogs.Person.Add
{
    [Serializable]
    public class Option2Dialog : IDialog<string>
    {
        private int attempts = 3;

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("¿Qué desea realizar?");
            context.Wait(this.MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            /* If the message returned is a valid name, return it to the calling dialog. */
            if ((message.Text != null) && (message.Text.Trim().Length > 0))
            {
                /* Completes the dialog, removes it from the dialog stack, and returns the result to the parent/calling
                    dialog. */

                WitClient clienteWit = new WitClient("5XEDASHHCHADD6B4HL5NV7O7OPGZNZ4T");

                Message messagewit = clienteWit.GetMessage(message.Text);

                if (messagewit.entities.ToList().FindAll(x => x.Key == "intent").Count > 0)
                {
                    if (messagewit.entities.ToList().FindAll(x => x.Value.FirstOrDefault().value.ToString() == "saludo").Count > 0)
                    {
                        message.Speak = "Hola";
                        message.InputHint = InputHints.AcceptingInput;
                        await context.PostAsync("Hola");
                        //await connector.Conversations.ReplyToActivityAsync(reply2);
                    }
                }
                //context.Done(message.Text);
                context.Wait(this.MessageReceivedAsync);
            }
            /* Else, try again by re-prompting the user. */
            else
            {
                --attempts;
                if (attempts > 0)
                {
                    await context.PostAsync("I'm sorry, I don't understand your reply. What is your name (e.g. 'Bill', 'Melinda')?");
                    context.Wait(this.MessageReceivedAsync);
                }
                else
                {
                    /* Fails the current dialog, removes it from the dialog stack, and returns the exception to the 
                        parent/calling dialog. */
                    context.Fail(new TooManyAttemptsException("Message was not a string or was an empty string."));
                }
            }

        }
    }
}