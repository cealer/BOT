﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using com.valgut.libs.bots.Wit;
using com.valgut.libs.bots.Wit.Models;
using System.Linq;
using System.Threading;
using BotPwa.Dialogs.Person.Add;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using BotPwa.Models;

namespace BotPwa.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        //private RecResponse recResponse;
        private string surname;
        private int age;
        public string uri = "https://monitor20180107071656.azurewebsites.net/";

        //public Task StartAsync(IDialogContext context)
        //{
        //    context.Wait(MessageReceivedAsync);

        //    return Task.CompletedTask;
        //}

        public async Task StartAsync(IDialogContext context)

        {

            /* Wait until the first message is received from the conversation and call MessageReceviedAsync 

             *  to process that message. */

            context.Wait(this.MessageReceivedAsync);

        }

        //private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        //{
        //    var activity = await result as Activity;

        //    // calculate something for us to return
        //    int length = (activity.Text ?? string.Empty).Length;

        //    WitClient clienteWit = new WitClient("5XEDASHHCHADD6B4HL5NV7O7OPGZNZ4T");

        //    Message message = clienteWit.GetMessage(activity.Text);

        //    if (message.entities.ToList().FindAll(x => x.Key == "intent").Count > 0)
        //    {
        //        if (message.entities.ToList().FindAll(x => x.Value.FirstOrDefault().value.ToString() == "saludo").Count > 0)
        //        {
        //            Activity reply2 = activity.CreateReply($"Hola! {activity.From.Name}, estoy a su disposición.");
        //            reply2.Speak = "Hola";
        //            reply2.InputHint = InputHints.AcceptingInput;
        //            await context.PostAsync(reply2);
        //            //await connector.Conversations.ReplyToActivityAsync(reply2);
        //        }
        //        else
        //        {

        //        }
        //    }
        //    //Activity reply = activity.CreateReply($"Hola! {activity.From.Name}, estoy a su disposición.");
        //    //reply.Speak = "This is the text that will be spoken.";
        //    //reply.InputHint = InputHints.AcceptingInput;

        //    if (activity.Text.StartsWith("persona"))
        //    {
        //        await context.SayAsync(text: "Datos de la persona", speak: "Datos de la persona");
        //    }
        //    else if (activity.Text.StartsWith("agregar persona"))
        //    {
        //        await context.Forward(new PersonDialog(), this.ResumeAfterPersonDialog, activity, CancellationToken.None);

        //    }
        //    else
        //    {
        //        var photo = activity.Attachments;

        //        await context.PostAsync(text: activity.Attachments[0].Name);

        //        await context.SayAsync(text: "Foto recibidida", speak: "Foto recibida, se agregará al sistema");
        //    }

        //    // return our reply to the user
        //    //await context.PostAsync($"You sent {activity.Text} which was {length} characters");

        //    context.Wait(MessageReceivedAsync);
        //}

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            /* When MessageReceivedAsync is called, it's passed an IAwaitable<IMessageActivity>. To get the message,
             *  await the result. */
            var message = await result;
            await this.SendWelcomeMessageAsync(context);

        }

        private async Task SendWelcomeMessageAsync(IDialogContext context)
        {
            await context.PostAsync("Hola! ¿En qué puedo ayudarte? ");

            context.Call(new OptionDialog(), this.OptionDialogResumeAfter);

        }

        private async Task OptionDialogResumeAfter(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                //WIT.AI

                RecResponse recResponse = new RecResponse();

                WitClient clienteWit = new WitClient("5XEDASHHCHADD6B4HL5NV7O7OPGZNZ4T");

                var r = result.GetAwaiter().GetResult();

                Message message = clienteWit.GetMessage(r);

                if (message.entities.ToList().FindAll(x => x.Key == "intent").Count > 0)
                {
                    if (message.entities.ToList().FindAll(x => x.Value.FirstOrDefault().value.ToString() == "saludo").Count > 0)
                    {
                        //Activity reply2 = activity.CreateReply($"Hola! {activity.From.Name}, estoy a su disposición.");
                        //reply2.Speak = "Hola";
                        //reply2.InputHint = InputHints.AcceptingInput;
                        //await context.PostAsync(reply2);
                        //await connector.Conversations.ReplyToActivityAsync(reply2);
                    }

                    else if (message.entities.ToList().FindAll(x => x.Value.FirstOrDefault().value.ToString() == "asistencia").Count > 0)
                    {
                        if (message.entities.ToList().FindAll(x => x.Key == "contact").Count > 0)
                        {
                            var contact = message.entities.ToList().FindAll(x => x.Key == "contact").FirstOrDefault();
                            
                            recResponse = await GetAssistancePerson(context.Activity.From.Id,contact.Value[0].value.ToString(), context);
                                
                            if (recResponse != null)
                            {
                                CarouselCardsDialog aux = new CarouselCardsDialog();
                                aux._recResponse = recResponse;
                                await aux.MessageReceivedAsync(context, null);
                            }
                            else
                            {
                                await context.PostAsync("No se encontraron registro");
                            }
                        }
                    }

                    else
                    {
                        await context.PostAsync("No entendí lo que me solicitó! :(");  
                    }
                }
            
            }

            catch (TooManyAttemptsException)
            {
                await context.PostAsync("Lo siento estoy teniendo problemas con lo que me solicitaste, intentalo de nuevo!");
                await this.SendWelcomeMessageAsync(context);
            }
        }

        private async Task<RecResponse> GetAssistancePerson(string id,string contact, IDialogContext context)
        {
            using (var client = new HttpClient())
            {
                //Desarrollo
                client.BaseAddress = new Uri(uri);

                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");

                client.DefaultRequestHeaders.Accept.Add(contentType);

                //Desarrollo
                HttpResponseMessage res = await
                    client.GetAsync($"Recognitions/GetPerson/{id}/{contact}");

                string stringData = res.Content.ReadAsStringAsync().Result;

                RecResponse data = JsonConvert.DeserializeObject<RecResponse>(stringData);

                if (data != null)
                {
                    return data;
                }

                return null;
            }
        }
        
    }
}