﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BotPwa.Models
{
    public class DescriptionRec
    {
        public string DesId { get; set; }

        public string RecId { get; set; }

        public string Description { get; set; }

        public virtual Recognition Rec { get; set; }
    }
}