﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BotPwa.Models
{
    public class RecResponse
    {
        public Recognition recognition { get; set; }
        public Camara camara { get; set; }
        public Subsidiary subsidiary { get; set; }
        public Company company { get; set; }
        public Person person { get; set; }
        public List<DescriptionRec> descriptions { get; set; }
    }
}