﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BotPwa.Models
{
    public class Person
    {
        public string perId { get; set; }
        public string perName { get; set; }
        public string perLastName { get; set; }
        public string identificationDocument { get; set; }
        public string perEmail { get; set; }
        public string perPath { get; set; }
        public object perPhone { get; set; }
        public DateTime perDate { get; set; }
        public string perGender { get; set; }
        public DateTime perDateBirth { get; set; }
        public string subId { get; set; }
        public string catId { get; set; }
        public bool perEnabled { get; set; }
        public string perPhoto { get; set; }
        public string userId { get; set; }
        public string fullName { get; set; }
    }
}