﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BotPwa.Models
{
    public class Camara
    {
        public string camId { get; set; }
        public string camDescription { get; set; }
        public object camLongitude { get; set; }
        public DateTime camDate { get; set; }
        public string subId { get; set; }
        public string camSource { get; set; }
        public bool camEnabled { get; set; }
        public object camLatitude { get; set; }
        public string userId { get; set; }
        public string timeZone { get; set; }
    }
}