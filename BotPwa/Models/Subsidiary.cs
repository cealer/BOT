﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BotPwa.Models
{
    public class Subsidiary
    {
        public string subId { get; set; }
        public string subDescription { get; set; }
        public DateTime subDate { get; set; }
        public string comId { get; set; }
        public string subNameLocation { get; set; }
        public bool subEnabled { get; set; }
        public object subLatitude { get; set; }
        public object subLongitude { get; set; }
        public string userId { get; set; }
        public List<object> detailsAlarms { get; set; }
    }

}