﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BotPwa.Models
{
    public class Recognition
    {
        public string recId { get; set; }
        public string perId { get; set; }
        public string recPath { get; set; }
        public DateTime recDate { get; set; }
        public string subId { get; set; }
        public string camId { get; set; }
        public bool recEnabled { get; set; }
        public string recPathFull { get; set; }
    }
}