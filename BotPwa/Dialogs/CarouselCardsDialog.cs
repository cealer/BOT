﻿using BotPwa.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BotPwa.Dialogs
{
    //[Serializable]

    public class CarouselCardsDialog : IDialog<object>
    {
        public RecResponse _recResponse;

        public async Task StartAsync(IDialogContext context)
        {

            context.Wait(this.MessageReceivedAsync);

        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var reply = context.MakeMessage();

            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;

            reply.Attachments = GetCardsAttachments(_recResponse);

            await context.PostAsync(reply);

            context.Wait(this.MessageReceivedAsync);
        }

        private static IList<Attachment> GetCardsAttachments(RecResponse recResponse)
        {
            StringBuilder str = new StringBuilder();
            str.Append($"Fue captado por la cámara {recResponse.camara.camDescription} en {recResponse.subsidiary.subDescription}, {recResponse.company.comDescription} a la hora de {recResponse.recognition.recDate}");

            if (recResponse.descriptions != null && recResponse.descriptions.Count > 0)
            {
                foreach (var item in recResponse.descriptions)
                {
                    str.AppendLine(item.Description);
                }
            }

            return new List<Attachment>()

            {

                GetHeroCard(

                    "Último reconocimiento",

                    recResponse.person.fullName,

                    str.ToString(),

                    new CardImage(url: recResponse.recognition.recPath),

                    new CardAction(ActionTypes.OpenUrl, "Más detalles", value: $"https://monitor20180107071656.azurewebsites.net/Recognitions/Details/{recResponse.recognition.recId}")),

                GetHeroCard(

                    "Último reconocimiento",

                    recResponse.person.fullName,
                    str.ToString()
,
                    new CardImage(url: recResponse.recognition.recPathFull),

                    new CardAction(ActionTypes.OpenUrl, "Más detalles", value: $"https://monitor20180107071656.azurewebsites.net/Recognitions/Details/{recResponse.recognition.recId}")),
                
                //GetThumbnailCard(

                //    "DocumentDB",

                //    "Blazing fast, planet-scale NoSQL",

                //    "NoSQL service for highly available, globally distributed apps—take full advantage of SQL and JavaScript over document and key-value data without the hassles of on-premises or virtual machine-based cloud database options.",

                //    new CardImage(url: "https://docs.microsoft.com/en-us/azure/documentdb/media/documentdb-introduction/json-database-resources1.png"),

                //    new CardAction(ActionTypes.OpenUrl, "Learn more", value: "https://azure.microsoft.com/en-us/services/documentdb/")),

                //GetThumbnailCard(

                //    "Cognitive Services",

                //    "Build powerful intelligence into your applications to enable natural and contextual interactions",

                //    "Enable natural and contextual interaction with tools that augment users' experiences using the power of machine-based intelligence. Tap into an ever-growing collection of powerful artificial intelligence algorithms for vision, speech, language, and knowledge.",

                //    new CardImage(url: "https://azurecomcdn.azureedge.net/cvt-68b530dac63f0ccae8466a2610289af04bdc67ee0bfbc2d5e526b8efd10af05a/images/page/services/cognitive-services/cognitive-services.png"),

                //    new CardAction(ActionTypes.OpenUrl, "Learn more", value: "https://azure.microsoft.com/en-us/services/cognitive-services/")),

            };

        }

        private static Attachment GetHeroCard(string title, string subtitle, string text, CardImage cardImage, CardAction cardAction)

        {

            var heroCard = new HeroCard

            {

                Title = title,

                Subtitle = subtitle,

                Text = text,

                Images = new List<CardImage>() { cardImage },

                Buttons = new List<CardAction>() { cardAction },

            };



            return heroCard.ToAttachment();

        }



        private static Attachment GetThumbnailCard(string title, string subtitle, string text, CardImage cardImage, CardAction cardAction)

        {

            var heroCard = new ThumbnailCard

            {

                Title = title,

                Subtitle = subtitle,

                Text = text,

                Images = new List<CardImage>() { cardImage },

                Buttons = new List<CardAction>() { cardAction },

            };



            return heroCard.ToAttachment();

        }

    }
}